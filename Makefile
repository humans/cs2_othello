CC          = g++
CFLAGS      = -Wall -ansi -pedantic -ggdb
OFLAGS			= -lrt
OBJS        = humansplayer.o wrapper.o board.o node.o tree.o
PLAYERNAME  = humans

all: $(PLAYERNAME) testgame
	
$(PLAYERNAME): $(OBJS)
	$(CC) -o $@ $^ $(OFLAGS)
        
TestGame: testgame.o
	$(CC) -o $@ $^ $(OFLAGS)
        
%.o: %.cpp
	$(CC) -c $(CFLAGS) -x c++ $< -o $@
	
java:
	make -C java/

cleanjava:
	make -C java/ clean

clean:
	rm -f *.o $(PLAYERNAME) testgame		
	
.PHONY: java
