#ifndef __TREE_H__
#define __TREE_H__

#include "common.h"
#include <map>
#include <time.h>
#include "node.h"
#include <iostream>
using namespace std;

class Tree
{
public:
  Tree(Side firstPlayer);
  ~Tree();
  void makeMove(Move* m);
  Move* makeBestMove(Side s);
  int spawnForAWhile(int maxSpawns);
  
private:
  Node* root;
  Side currentPlayer;
};

#endif
