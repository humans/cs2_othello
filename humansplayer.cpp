#include "humansplayer.h"

HumansPlayer::HumansPlayer(Side s)
{
  mySide = s;
  gameTree = new Tree((Side) -s);
  hasGone = false;
  spawnsPerMS = 4; //TODO: start with a good estimate for what it actually will be
}

HumansPlayer::~HumansPlayer()
{
  delete gameTree;
}

Move* HumansPlayer::doMove(Move* opponentsMove, int msLeft)
{
  struct timespec startTime, endTime;
  clock_gettime(CLOCK_REALTIME, &startTime);
  //cerr << "msLeft: " << msLeft << endl;
  if(opponentsMove != NULL) totalMovesMade++;
  gameTree->makeMove(opponentsMove);
  
  int spawnCount = 0;
  if(msLeft == 0)
  {
    spawnCount = 20000;
  }
  else if(totalMovesMade < 55)
  {
    spawnCount = msLeft * spawnsPerMS / (55 - totalMovesMade);
  }
  else
  {
    spawnCount = msLeft * spawnsPerMS * 2.0 / (60 - totalMovesMade);
  }

  if(spawnCount > 50000) spawnCount = 50000;

  //cerr << "Spawned a total of " << (spawnCount = gameTree->spawnForAWhile(spawnCount)) << " times" << endl;
  spawnCount = gameTree->spawnForAWhile(spawnCount);
  
  Move* retVal = gameTree->makeBestMove(mySide);
  clock_gettime(CLOCK_REALTIME, &endTime);
  float msTaken = (endTime.tv_sec - startTime.tv_sec) * 1E3 + (endTime.tv_nsec - startTime.tv_nsec) * 1E-6;
  spawnsPerMS = spawnCount / msTaken;
  //cerr << "That took " << msTaken << "ms; updating spawnsPerMS to " << spawnsPerMS << endl;
  
  if(retVal != NULL) totalMovesMade++;
  return retVal;
}
