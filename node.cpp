#include "node.h"

Node::Node(Board* b, Side s)
{
  myBoard = b;
  currentPlayer = s;
  childrenSpawned = false;
	scoreDeterminedHeuristically = false;
  scoreNeedsToBeUpdated = true;
}

Node::~Node()
{
  map<int, Node*>::iterator i;
  for(i = children.begin(); i != children.end(); i++)
  {
    delete i->second;
  }
  delete myBoard;
}

/* Return the score, using children if they exist and using direct evaluation
 * if they do not. The score of a node for a player forPlayer is how good it
 * looks to forPlayer to see that currentPlayer is looking at myBoard. */
int Node::getScore(Side forPlayer, Move** m)
{
  if(!scoreNeedsToBeUpdated && m == NULL) return scoreForBlack * (BLACK * forPlayer);
  scoreNeedsToBeUpdated = false;
  
  if(childrenSpawned)
  {
    if(children.size() > 0)
    {
      /* Determine score based on children. */
      
      /* The score for forPlayer is +/- the score for currentPlayer. The score
       * for currentPlayer is the max of all the children's scores for
       * currentPlayer, because currentPlayer is free to choose which child
       * will be realized. */
       
      map<int, Node*>::iterator i = children.begin();

      int scoreForCurrentPlayer =  i->second->getScore(currentPlayer, NULL);
      int mX = i->first % 8;
      int mY = i->first / 8;

      for(i++; i != children.end(); i++)
      {
        int childScore = i->second->getScore(currentPlayer, NULL);
        if(childScore > scoreForCurrentPlayer)
        {
          scoreForCurrentPlayer = childScore;
          mX = i->first % 8;
          mY = i->first / 8;
        }
      }
      
      if(m != NULL) *m = new Move(mX, mY);
      scoreForBlack = scoreForCurrentPlayer * (BLACK * currentPlayer);
    }
    else
    {
      // Game over. Determine score based on winner!
      if(m != NULL) *m = NULL;
      scoreForBlack = myBoard->winner() * BLACK * (INT_MAX - 10);
    }
  }
  else
  {
    // Determine score heuristically.
    if(!scoreDeterminedHeuristically) determineScoreHeuristically();
    if(m != NULL) *m = NULL; 
  }
  
  return scoreForBlack * (BLACK * forPlayer);
}

void Node::determineScoreHeuristically()
{
  scoreDeterminedHeuristically = true;
  scoreForBlack = myBoard->evaluateForBlack(currentPlayer);
}

void Node::actualSpawn(bool secondTry)
{
  Board* nextChildBoard;
  Move* associatedMove;
  while((nextChildBoard = myBoard->nextHypotheticalFuture(currentPlayer, &associatedMove)) != NULL)
  {
    Node* newNode = new Node(nextChildBoard, (Side) -currentPlayer);
    children[associatedMove->x + 8 * associatedMove->y] = newNode;
    delete associatedMove;
  }
  
  if(children.empty() && !secondTry)
  {
    // Oh noes! No moves available... Maybe there are moves available for the other player?
    currentPlayer = (Side) -currentPlayer;
    myBoard->resetHypotheticalFutures();
    actualSpawn(true);
  }
}

int Node::spawnChildren(int maxSpawns)
{
  int totalSpawns = 0;
  
  if(maxSpawns > 0)
  {
    if(!childrenSpawned)
    {
      childrenSpawned = true;
      actualSpawn(false);
      childIndex = children.size();
      totalSpawns++;
      //cerr << "Actual spawn here: now I have " << children.size() << endl;
    }
    
    if(children.size() > 0)
    {
      if(childIndex == children.size())
      {
        childIndex = 0;
        childIterator = children.begin();
      }
      
      while(totalSpawns < maxSpawns && childIndex < children.size())
      {
        totalSpawns += childIterator->second->spawnChildren((maxSpawns - totalSpawns) / (children.size() - childIndex));

        childIterator++; // I sure hope I don't screw stuff up and do this when I oughtn't
        childIndex++;
      }
    }
  }
  
  //if(totalSpawns != maxSpawns) cerr << "ALERT: totalSpawns == " << totalSpawns << "; maxSpawns == " << maxSpawns << endl;
  if(totalSpawns > 0) scoreNeedsToBeUpdated = true;
  
  return totalSpawns;
}
