#ifndef __NODE_H__
#define __NODE_H__

#include "common.h"
#include "board.h"
#include <limits.h>
#include <map>
#include <list>
#include <iostream>

using namespace std;

class Node
{
private:
  bool scoreDeterminedHeuristically;
  bool scoreNeedsToBeUpdated;
  int scoreForBlack;
  void determineScoreHeuristically();
  map<int, Node*>::iterator childIterator;
  int childIndex;
  void actualSpawn(bool secondTry);
  
public:
  Node(Board* b, Side s);
  ~Node();

  Board* myBoard;
  Side currentPlayer; // The hypothetical player associated with this node.
  map<int, Node*> children; // the int encodes a move.
  bool childrenSpawned;
  
  int getScore(Side forPlayer, Move** m); // forPlayer is the actual player running all this nonsense.
  int spawnChildren(int maxSpawns);
};

#endif
