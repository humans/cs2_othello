#ifndef __HUMANSPLAYER_H__
#define __HUMANSPLAYER_H__

#include <iostream>
#include "common.h"
#include "tree.h"
#include <time.h>

using namespace std;

class HumansPlayer
{
public:
  HumansPlayer(Side side);
  ~HumansPlayer();
  
  Move* doMove(Move *opponentsMove, int msLeft);
  
private:
  Side mySide;
  Tree* gameTree;
  int totalMovesMade;
  bool hasGone;
  float spawnsPerMS;
};

#endif
