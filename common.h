#ifndef __COMMON_H__
#define __COMMON_H__

#define max(x, y)			((x) > (y) ? (x) : (y))

enum Side {
    WHITE = 1, BLACK = -1, NONE = 0
};

class Move {
   
public:
    int x, y;
    Move(int x, int y) {
        this->x = x;
        this->y = y;        
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
};

#endif
