Team: Humans

**Member contribution at end**

We wrote a feed-forward single-hidden layer artificial neural network with 42 hidden nodes and a learning coefficient that decayed exponentially from 0.1 to 0.001.  We trained it using back-propagation algorithm on tournament data from the WTHOR database, which we parsed gameboards out of a binary file containing move sequences.  The reward function that we trained it with mapped each gameboard to the eventual score differential associated with that board.  We iterated through the data ~1000 times, but decided to go with the 100th generation weights due to overfitting problems.  

This is the heuristic used, in conjunction with the game tree that is constructed in a breadth-first manner so as to allow the construction of partial layers for finer tuning of the time spent on each turn.  The portions of the game tree that are still potential futures are preserved in memory from one turn to the next.

Although this might end up working in our favor, upon reflection we would reconsider this path and devote more time to developing an opening book/using alpha-beta pruning as opposed to relying on the neural network.  We don't fully understand some aspects of the neural network because when it is spawns a smaller tree it performs better, confusingly.  

One of the concepts we struggled with was enabling the AI to spawn a maximum number of nodes during the time alloted for the game.  It was hard to predict early in the game how much time the rest of the game will take because the entire tree is built before the last few moves are made (meaning no spawning occurs and the moves are made immediately.)  We ended up capping the number of spawns per move at 50,000.  This is to prevent the neural network issues that were raised when creating a large tree.

Will wrote the neural network, and the minimax tree in week two.  Catherine handled the spawning related to the time alloted to a game, and the framework from week one.
 


