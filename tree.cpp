#include "tree.h"

Tree::Tree(Side firstPlayer)
{
  root = new Node(new Board(NULL), BLACK);
  currentPlayer = firstPlayer;
}

Tree::~Tree()
{
  delete root;
}

int Tree::spawnForAWhile(int maxSpawns)
{
  return root->spawnChildren(maxSpawns);
}

void Tree::makeMove(Move* m)
{
  if(m != NULL)
  {
    //cerr << "Current player is " << currentPlayer << " and he is going at " << m->x << ", " << m->y << " yielding the following board" <<  endl;
    // Move down the tree, deleting the branches except the one corresponding to m.
    if(!root->childrenSpawned) root->spawnChildren(1); // the root MUST spawn.
    if(root->children.count(m->x + 8 * m->y) != 1)
    {
      cerr << "ERROR: impossible move?!" << endl;
      cerr << "The only moves I have listed are: " << endl;
      for(map<int, Node*>::iterator i = root->children.begin(); i != root->children.end(); i++)
      {
        cerr << "   " << i->first % 8 << ", " << i->first / 8 << endl;
      }
      cerr << "There are a total of " << root->children.size() << endl;
      cerr << "Note: has root spawned children? " << root->childrenSpawned << endl;
      return;
    }
    Node* newRoot = root->children[m->x + 8 * m->y];
    root->children.erase(m->x + 8 * m->y); // this way, the good branch won't be deleted when we delete root
    delete root;
    root = newRoot;
    
    //cerr << "Current board: " << endl;
    //root->myBoard->print();
  }
}

/* Return the move made */
Move* Tree::makeBestMove(Side s)
{
  Move *m = NULL;
  currentPlayer = root->currentPlayer; //probably switching players, but you never know
  if(currentPlayer == s) root->getScore(currentPlayer, &m); // if it's not my turn I'll pass...
  if(m != NULL)
  {
    makeMove(m);
  }
  else
  {
    cerr << "Passing. Note: root has " << root->children.size() << " children. currentPlayer == " << currentPlayer << endl;
  }
  return m;
}
