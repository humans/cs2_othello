#ifndef __BOARD_H__
#define __BOARD_H__

#include "common.h"
#include <math.h>
#include <bitset>
#include <iostream>
using namespace std;

class Board
{
  
private:
  bitset<64> black;
  bitset<64> white;
  int searchX;
  int searchY;
  Board* copy();
  bool onBoard(int x, int y);
  float sigmoid(float in);
  
public:
  Board(Board* copyFrom);
  ~Board();
  Side occupant(int x, int y);
  Board* nextHypotheticalFuture(Side s, Move** m);
  void resetHypotheticalFutures();
  int evaluateForBlack(Side currentPlayer);
  int evaluateForBlackNaively();
  void set(Side s, int x, int y);
  Side winner();
  void print();
};

#endif
